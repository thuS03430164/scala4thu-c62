package apps

import cc.{Fraction, ListNode}

/**
  * Created by mark on 16/04/2017.
  */
object HelloWorld extends App{
  print("Enter your name: ")
  val somebody= readLine()

  println(s"Hello $somebody !!")
////  args.map()
//  args.foreach(somebody=>{
//    println(s"Hello $somebody !!")
//  })


}

object FractionApp extends App {
  val frac = Fraction(1,2) plus  Fraction(1,3)
  val frac2 =Fraction(1,2)==Fraction(3,6).reduce()
  //val frac3=Fraction(3,0)
  val listNode=ListNode(1,ListNode(2,ListNode(3,ListNode(4,null))))

  println(listNode)
}

